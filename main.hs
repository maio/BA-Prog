{-
# Copyright (C) 2021-2022 Luca Alessio Maio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
-}

import Data.List
import Data.Bifunctor
import Data.Ratio
import qualified System.Random as R

data Expr a = Var a
            | Lambda a (Expr a)
            | Application (Expr a) (Expr a)
            | Letrec [Assignment a] (Expr a)
            | Prob (Expr a) (Expr a) Rational
            | RM deriving Eq

instance (Show a) => Show (Expr a) where
    show (Var v) = show v
    show (Lambda v e) = "(\\" ++ show v ++ "." ++ show e ++ ")"
    show (Application e1 e2) = "(" ++ show e1 ++ " " ++ show e2 ++ ")"
    show (Letrec env e) = "(let " ++ showEnvs env ++ " in " ++ show e ++ ")"
    show (Prob e1 e2 p) = "{prob[" ++ show p ++ "] " ++ show e1 ++ " " ++ show e2 ++ "}"
    show RM = "RM"
data Assignment a = Assig a (Expr a) deriving (Eq,Show)

showEnvs :: Show a => [Assignment a] -> String
showEnvs env = intercalate "; " (map showEnv env)
    where showEnv (Assig v e) = show v ++ "=" ++ show e

data ProbTree a = PLeaf (Expr a) Integer
                | PNode (Expr a)    (Either String (ProbTree a, Rational))
                                    (Either String (ProbTree a, Rational))
                                    deriving Eq
instance (Show a) => Show (ProbTree a) where
    show (PLeaf e i) = "Leaf " ++ show e ++ " number of steps: " ++ show i
    show (PNode e (Right (tl,pl)) (Right (tr,pr))) =
        "Node " ++ "[" ++ show e ++ "] " ++ "(" ++ show tl ++ ", "
        ++ show pl ++ ")" ++ " " ++ "(" ++ show tr ++ ", " ++ show pr ++ ")"
    show (PNode _ (Left s) (Left _)) = show s
    show _ = error "This Tree should not be possible"

data Strategy = Name | Value | Need

data Result a = Branch Rational a a | Next a

newtype StringExpr = SE String deriving Eq
instance Show StringExpr where
    show (SE s) = s

type EExpr a = Either String (Expr a)
type ProbDistribution a = [(EExpr a, Rational)]

--test statements for debuging

comp = do
        xs' <- monteCarlo treeGrowth True Need 100
        return $ compA (reduceChurch xs') xs' []
    where
        compA [] [] res = res
        compA ((Right ec, _):xsc) ((Right en, _):xsn) res
            | churchToInt ec == churchToIntNeed en [] = compA xsc xsn res
            | otherwise = compA xsc xsn ((churchToInt ec, churchToIntNeed en [], en):res)
        compA (xc:xsc) (xn:xsn) res = compA xsc xsn res

stepAvrg s = div (sum [countchoosekofn k n s | k <- [1,2,3], n <- [4,5,6,7]]) 12

amountOfDoublePossibilities :: Integer -> Integer -> Integer
amountOfDoublePossibilities k n = n^k - factorial (n-k) n

factorial :: Integer -> Integer -> Integer
factorial k n = product [(k+1)..n]

omega :: Expr StringExpr
omega = Application (Lambda (SE "x") (Application (Var (SE "x")) (Var (SE "x")))) (Lambda (SE "x") (Application (Var (SE "x")) (Var (SE "x"))))

cpeTest :: Expr String
cpeTest = Letrec [Assig "x1" (Application (Var "x3") (Var "x2")), Assig "x2" (Application (Application (Var "x4") (Var "x5")) (Var "x6")), Assig "x4" (Var "x7"), Assig "x8" (Lambda "v1" (Application (Var "x9") (Var "x10"))), Assig "x7" (Var "x8")] (Var "x1")

cpeTestEnv :: [Assignment String]
cpeTestEnv = [Assig "x1" (Application (Var "x3") (Var "x2")), Assig "x2" (Application (Application (Var "x4") (Var "x5")) (Var "x6")), Assig "x4" (Var "x7"), Assig "x8" (Lambda "v1" (Application (Var "x9") (Var "x10"))), Assig "x7" (Var "x8")]

lleteTest :: Expr String
lleteTest = Letrec [Assig "x1" (Application (Var "x3") (Var "x2")), Assig "x2" (Application (Application (Var "x4") (Var "x5")) (Var "x6")), Assig "x4" (Letrec [Assig "x8" (Lambda "v1" (Application (Var "x9") (Var "x10"))), Assig "x7" (Var "x8")] (Var "x7"))] (Var "x1")

reductionTest :: Expr String
reductionTest = Letrec [Assig "x" (Application (Lambda "u" (Var "u")) (Lambda "w" (Var "w")))] (Application (Lambda "y" (Var "y")) (Var "x"))

probTest :: Expr String
probTest = Application (Lambda "x" (Var "x")) (Prob (Var "y") (Prob (Lambda "z" (Var "z")) (Var "u") 0.5) 0.5)

strategyTest :: Expr StringExpr
strategyTest = Letrec [Assig (SE "x1") (Prob omega (Lambda (SE "y") (Var (SE "y"))) 0.05)] (Var (SE "x1"))

needStratTest :: Expr String
needStratTest = Letrec [Assig "x" (Application (Var "y") (Var "y")), Assig "y" (Lambda "z" (Application (Var "z") (Var "z")))] (Var "x")

applicTest :: Expr String
applicTest = Letrec [Assig "x" (Lambda "w" (Prob (Var "w") (Var "v") 0.5))] (Application (Var "x") (Var "u"))

goatProblemSwitch :: Expr String
goatProblemSwitch = Prob (Var "Lose") (Var "Win") 0.33

goatProblemNoSwitch :: Expr String
goatProblemNoSwitch = Prob (Var "Win") (Var "Lose") 0.33

goatProblemProbSwitch :: Expr String
goatProblemProbSwitch = Prob (Prob (Var "Win") (Var "Lose") 0.5) (Prob (Var "Win") (Var "Lose") 0.5) 0.33

zero :: Expr StringExpr
zero = Lambda (SE "f") (Lambda (SE "x") (Var (SE "x")))

one :: Expr StringExpr
one = Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Var (SE "x"))))

two :: Expr StringExpr
two = Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "f")) (Var (SE "x")))))

three :: Expr StringExpr
three = Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Var (SE "x"))))))

four :: Expr StringExpr
four = Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Var (SE "x")))))))

five :: Expr StringExpr
five = Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Application (Var (SE "f")) (Var (SE "x"))))))))

succL :: Expr StringExpr
succL = Lambda (SE "n") (Lambda (SE "f") (Lambda (SE "x") (Application (Var (SE "f")) (Application (Application (Var (SE "n")) (Var (SE "f"))) (Var (SE "x"))))))

plusL :: Expr StringExpr
plusL = Lambda (SE "m") (Lambda (SE "n") (Application (Application (Var (SE "n")) succL) (Var (SE "m"))))

ycomb :: Expr StringExpr
ycomb = Lambda (SE "f") (Application  (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "x")) (Var (SE "x")))))
                                 (Lambda (SE "x") (Application (Var (SE "f")) (Application (Var (SE "x")) (Var (SE "x"))))))

end :: Expr StringExpr
end = Prob (Var (SE "n")) zero 0.7

prob1to5 :: Expr StringExpr
prob1to5 = Prob one (Prob two (Prob three (Prob four five (24 % 31)) (38 % 69)) (8 % 31)) (7 % 100) -- 0.07 für 1 und 5, 0.24 für 2 und 4, 0.38 für 3, 

probFunc :: Expr StringExpr
probFunc = Lambda (SE "f") (Lambda (SE "n") (Prob (Application (Var (SE "f")) (Application (Application plusL prob1to5) (Var (SE "n")))) end 0.8)) --should convergenz

treeGrowth :: Expr StringExpr
treeGrowth = Application    (Application ycomb
                                        probFunc)
                            zero

true :: Expr StringExpr
true = Lambda (SE "x") (Lambda (SE "y") (Var (SE "x")))

false :: Expr StringExpr
false = Lambda (SE "x") (Lambda (SE "y") (Var (SE "y")))

pair :: Expr StringExpr
pair = Lambda (SE "a") (Lambda (SE "b") (Lambda (SE "f") (Application (Application (Var (SE "f")) (Var (SE "a"))) (Var (SE "b")))))

nTuple :: Integer -> Expr StringExpr
nTuple n = nTupRec n [] binders
    where
        nTupRec 0 collect _ = Lambda (SE "f") (makeApp collect)
        nTupRec n collect (name:bind) =
            Lambda name (nTupRec (n-1) (name:collect) bind)
        makeApp [name] = Application (Var (SE "f")) (Var name)
        makeApp (name:xs) = Application (makeApp xs) (Var name)

kthOfn :: Integer -> Integer -> Expr StringExpr
kthOfn k n =
    Lambda (SE "t") (Application (Var (SE "t")) (kthRec k n binders [] False))
    where
        kthRec _ 0 _ _ False = error "n must be between 1 and k"
        kthRec _ 0 _ [nth] _ = Var nth
        kthRec 1 n (name:bind) nth False =
            Lambda name (kthRec 0 (n-1) bind (name:nth) True)
        kthRec _ n (name:bind) nth True =
            Lambda name (kthRec 0 (n-1) bind nth True)
        kthRec k n (name:bind) nth False =
            Lambda name (kthRec (k-1) (n-1) bind nth False)

fstCh :: Expr StringExpr
fstCh = Lambda (SE "t") (Application (Var (SE "t")) true)

sndCh :: Expr StringExpr
sndCh = Lambda (SE "t") (Application (Var (SE "t")) false)

choicePool :: Integer -> Expr StringExpr
choicePool n = makeProbExpr $ uniform [Var (SE (show i)) | i <- [1..n]]

chooseTwo n =
    let cpn = choicePool n
    in Application (Application pair cpn) cpn

toChurch :: Integer -> Expr StringExpr
toChurch n = Lambda (SE "f") (Lambda (SE "x") (toChurchRec n))
    where
        toChurchRec :: Integer -> Expr StringExpr
        toChurchRec 0 = Var (SE "x")
        toChurchRec m = Application (Var (SE "f")) (toChurchRec (m-1))

makeProbExpr :: [(Expr a, Rational)] -> Expr a
makeProbExpr xs =
    if isDist xs
        then makeRec xs (1 % 1)
        else error "sum of probabilities must be 1"
    where
        makeRec :: [(Expr a, Rational)] -> Rational -> Expr a
        makeRec [(e,_)] _ = e
        makeRec ((e,p):xs) pOld =
            let pNew = p / pOld
            in Prob e (makeRec xs (pOld*(1-pNew))) pNew

isDist :: [(a, Rational)] -> Bool
isDist xs = 1.0 == foldl' (+) 0.0 (map snd xs)

uniform :: [Expr a] -> [(Expr a, Rational)]
uniform xs =
    let p = toRational (1 % length xs)
    in zip xs $ repeat p

fromPair :: ProbDistribution StringExpr -> [(Either String (Expr StringExpr, Expr StringExpr), Rational)]
fromPair [] = []
fromPair ((Right e, p):xs) =
    case testReduceDistGC (Application fstCh e) of
        [(Right e', p')] -> case testReduceDistGC (Application sndCh e) of
                                [(Right e'', p'')] -> (Right (e', e''), p) : fromPair xs
                                _ -> error $ "The expression" ++ show e ++ "might not be a pair"
        _ -> error $ "The expression" ++ show e ++ "might not be a pair"
fromPair ((Left s,p):xs) = (Left s,p) : fromPair xs

fromnTuple :: Integer -> ProbDistribution StringExpr -> [(Either String [Expr StringExpr], Rational)]
fromnTuple _ [] = []
fromnTuple n ((Right e, p):xs) = (Right (fromnTupleRec n n e), p) : fromnTuple n xs
    where
        fromnTupleRec _ 0 _ = []
        fromnTupleRec n k e =
            case testReduceStrategyDistGC (Application (kthOfn k n) e) Need of
                [(Right e', p')] -> e' : fromnTupleRec n (k-1) e
                _ -> error $ "The expression" ++ show e ++ "might not be a " ++ show n ++ "-tuple"
fromnTuple n ((Left s,p):xs) = (Left s,p) : fromnTuple n xs

countfromTuple :: Strategy -> Integer -> ProbDistribution StringExpr -> Integer
countfromTuple _ _ [] = 0
countfromTuple s n ((Right e, p):xs) = fromnTupleRec n n e + countfromTuple s n xs
    where
        fromnTupleRec _ 0 _ = 0
        fromnTupleRec n k e = countSteps (testReduceStrategyGC (Application (kthOfn k n) e) s) + fromnTupleRec n (k-1) e
countfromTuples s n ((Left _,_):xs) = countfromTuple s n xs

fromChurchPair :: ProbDistribution StringExpr -> [(Either String (Integer, Integer), Rational)]
fromChurchPair [] = []
fromChurchPair ((Right e, p):xs) =
    case testReduceDistGC (Application fstCh e) of
        [(Right e', p')] -> case testReduceDistGC (Application sndCh e) of
                                [(Right e'', p'')] -> (Right (churchToInt e', churchToInt e''), p) : fromChurchPair xs
                                _ -> error $ "The expression" ++ show e ++ "might not be a pair"
        _ -> error $ "The expression" ++ show e ++ "might not be a pair"
fromChurchPair ((Left s,p):xs) = (Left s, p) : fromChurchPair xs

fromnTupleChurch :: Integer -> ProbDistribution StringExpr -> [(Either String [Integer], Rational)]
fromnTupleChurch _ [] = []
fromnTupleChurch n ((Right e, p):xs) = (Right (fromnTupleChurchRec n n e), p) : fromnTupleChurch n xs
    where
        fromnTupleChurchRec _ 0 _ = []
        fromnTupleChurchRec n k e =
            case testReduceStrategyDistGC (Application (kthOfn k n) e) Need of
                [(Right e', p')] -> churchToInt e' : fromnTupleChurchRec n (k-1) e
                _ -> error $ "The expression" ++ show e ++ "might not be a " ++ show n ++ " tuple"
fromnTupleChurch n ((Left s,p):xs) = (Left s,p) : fromnTupleChurch n xs

allChurchNums :: [Expr StringExpr]
allChurchNums = [toChurch i | i <- [0..]]

choose2ofn :: Int -> ProbDistribution StringExpr
choose2ofn n =
    let pool = makeProbExpr $ uniform $ take n allChurchNums
    in testReduceDistGC $ Application (Application pair pool) pool

choosekofn :: Integer -> Integer -> [(Either String [Integer], Rational)]
choosekofn k n =
    let pool = makeProbExpr $ uniform $ take (fromIntegral n) allChurchNums
    in fromnTupleChurch k $ testReduceStrategyDistGC (choosekofnRec k k pool) Need
        where
            choosekofnRec k 1 pool = Application (nTuple k) pool
            choosekofnRec k ctr pool = Application (choosekofnRec k (ctr-1) pool) pool

countchoosekofn :: Integer -> Integer -> Strategy -> Integer
countchoosekofn k n s =
    let pool = makeProbExpr $ uniform $ take (fromIntegral n) allChurchNums
        reduce = testReduceStrategyGC (choosekofnRec k k pool) s
        count1 = countSteps reduce
        count2 = countfromTuple s k $ treeToDist reduce
    in count1 + count2
        where
            choosekofnRec k 1 pool = Application (nTuple k) pool
            choosekofnRec k ctr pool = Application (choosekofnRec k (ctr-1) pool) pool

choosekofnMC :: Integer -> Integer -> Integer -> Strategy -> IO [(Either String [Integer], Rational)]
choosekofnMC k n numRep s =
    let pool = makeProbExpr $ uniform $ take (fromIntegral n) allChurchNums
    in do
         xs <- monteCarlo (choosekofnRec k k pool) True s numRep
         return $ fromnTupleChurch k xs
        where
            choosekofnRec k 1 pool = Application (nTuple k) pool
            choosekofnRec k ctr pool = Application (choosekofnRec k (ctr-1) pool) pool

groupDistPair :: (Eq a, Eq b, Num c) => [(Either a (b, b), c)] -> [(Either a (b, b), c)]
groupDistPair xs = groupPairRec xs xs []
    where
        groupPairRec [] old res = res
        groupPairRec ((t@(Right (a,b)), p):xs) old res =
            case lookup t res of
                Nothing ->
                    case lookup (Right (b,a)) res of
                        Nothing -> groupPairRec xs old ((t,getProbSumPair t old):res)
                        _ -> groupPairRec xs old res
                _ -> groupPairRec xs old res
        groupPairRec ((t,p):xs) old res =
            case lookup t res of
                Nothing -> groupPairRec xs old ((t,getProbSumPair t old):res)
                _ -> groupPairRec xs old res

groupDistnTuple :: (Eq a, Eq b, Num c) => [(Either a [b], c)] -> [(Either a [b], c)]
groupDistnTuple xs = groupnTupleRec xs xs []
    where
        groupnTupleRec [] old res = res
        groupnTupleRec ((Right ts, p):xs) old res =
            if tupleIsIn (Right ts) res
                then groupnTupleRec xs old res
                else groupnTupleRec xs old ((Right ts,getProbSumTuple (Right ts) old):res)
        groupnTupleRec ((Left s, p):xs) old res =
            if tupleIsIn (Left s) res
                then groupnTupleRec xs old res
                else groupnTupleRec xs old ((Left s,getProbSumTuple (Left s) old):res)

getProbSumPair :: (Eq a, Eq b, Num c) => Either a (b,b) -> [(Either a (b,b),c)] -> c
getProbSumPair x [] = 0
getProbSumPair t@(Right (a,b)) ((Right (a', b'),p):xs)
    | (a == a' && b == b') || (a == b' && b == a') = p + getProbSumPair t xs
    | otherwise = getProbSumPair t xs
getProbSumPair t@(Left s) ((Left s', p):xs)
    | s == s' = p + getProbSumPair t xs
    | otherwise = getProbSumPair t xs
getProbSumPair t (_:xs) = getProbSumPair t xs

getProbSumTuple :: (Eq a, Eq b, Num c) => Either a [b] -> [(Either a [b],c)] -> c
getProbSumTuple x [] = 0
getProbSumTuple t@(Right ts) ((Right ts', p):xs)
    | isSetEqual ts ts' = p + getProbSumTuple t xs
    | otherwise = getProbSumTuple t xs
getProbSumTuple t@(Left s) ((Left s', p):xs)
    | s == s' = p + getProbSumTuple t xs
    | otherwise = getProbSumTuple t xs
getProbSumTuple t (_:xs) = getProbSumTuple t xs

isSetEqual :: Eq a => [a] -> [a] -> Bool
isSetEqual xs ys = null (xs \\ ys) && null (ys \\ xs)

tupleIsIn :: (Eq a, Eq b) => Either a [b] -> [(Either a [b], c)] -> Bool
tupleIsIn _ [] = False
tupleIsIn (Right ts) ((Right ts', p):xs) = isSetEqual ts ts' || tupleIsIn (Right ts) xs
tupleIsIn (Left s) ((Left s', p):xs) = s == s' || tupleIsIn (Left s) xs
tupleIsIn ts (_:xs) = tupleIsIn ts xs

eventOnkofn :: ((Either String [Integer], Rational) -> (Either String Bool, Rational)) -> Integer -> Integer -> [(Either String Bool, Rational)]
eventOnkofn event k n = groupDist $ map event $ choosekofn k n

eventOnkofnMC :: ((Either String [Integer], Rational) -> (Either String Bool, Rational)) -> Integer -> Integer -> Integer -> IO [(Either String Bool, Rational)]
eventOnkofnMC event k n numRep = do
                            xs <- choosekofnMC k n numRep Need
                            return $ groupDist $ map event xs

groupDist :: Eq a => [(Either String a, Rational)] -> [(Either String a, Rational)]
groupDist dist = groupDist' dist dist []
    where   groupDist' [] _ res = res
            groupDist' (ex@(e, pr):xs) old res =
                case lookup e res of
                    Nothing -> groupDist' xs old ((e, getProbSumGeneral e old):res)
                    Just p -> groupDist' xs old res

getProbSumGeneral :: (Eq a, Eq b, Num c) => Either a b -> [(Either a b,c)] -> c
getProbSumGeneral x [] = 0
getProbSumGeneral t ((t',p):xs)
    | t == t' = p + getProbSumGeneral t xs
    | otherwise = getProbSumGeneral t xs

eventDouble :: Eq a => (Either String [a], p) -> (Either String Bool, p)
eventDouble (e, p) = (fmap hasDuplicates e, p)

hasDuplicates :: Eq a => [a] -> Bool
hasDuplicates xs = go xs []
    where
        go [] res = False
        go (x:xs) res =
            elem x res || go xs (x:res)

countSteps :: ProbTree a -> Integer
countSteps (PLeaf _ ctr) = ctr
countSteps (PNode _ (Right (tl,_)) (Right (tr,_))) = countSteps tl + countSteps tr
countSteps (PNode _ (Right (tl,_)) _) = countSteps tl
countSteps (PNode _ _ (Right (tr,_))) = countSteps tr
countSteps _ = 0

testReduceGC :: Expr StringExpr -> ProbTree StringExpr
testReduceGC e = reduceNeed e True Need

testReduceDistGC :: Expr StringExpr -> ProbDistribution StringExpr
testReduceDistGC e = reduceNeedDist e True Need

testReduceStrategyGC :: Expr StringExpr -> Strategy -> ProbTree StringExpr
testReduceStrategyGC e = reduceNeed e True

testReduceStrategyDistGC :: Expr StringExpr -> Strategy -> ProbDistribution StringExpr
testReduceStrategyDistGC e = reduceNeedDist e True

testReduce :: Expr StringExpr -> ProbTree StringExpr
testReduce e = reduceNeed e False Need

testReduceDist :: Expr StringExpr -> ProbDistribution StringExpr
testReduceDist e = reduceNeedDist e False Need

testReduceStrategy :: Expr StringExpr -> Strategy -> ProbTree StringExpr
testReduceStrategy e = reduceNeed e False

testReduceGCX :: Expr StringExpr -> Strategy -> Integer -> ProbDistribution StringExpr
testReduceGCX e = reduceNeedXDist e True

testChurch :: Expr StringExpr -> [(Either String Integer, Rational)]
testChurch e =  groupDistChurch $ mapChurchToIntNeed $ testReduceDistGC e

monteCarloChurch :: Expr StringExpr -> Integer -> Strategy -> IO[(Either String Integer, Rational)]
monteCarloChurch e numRep s = do
                                dist <- monteCarlo e True s numRep
                                return $ (groupDistChurch . mapChurchToIntNeed . groupDistExpr) dist


-- |Fresh variable names for renaming expressions.
fresh :: [StringExpr]
fresh = [SE ("x_" ++ show i) | i <- [1..]]

fresh2 :: [StringExpr]
fresh2 = [SE ("y_" ++ show i) | i <- [1..]]

fresh3 :: [StringExpr]
fresh3 = [SE ("z_" ++ show i) | i <- [1..]]

binders :: [StringExpr]
binders = [SE ("b_" ++ show i) | i <- [1..]]

-- |rename expressions with new variable names to guarantee DVC. Taken from FFP from SoSe2020. Added cases for Letrec and Prob.
rename :: (Eq b) => Expr b -> [b] -> (Expr b, [b])
rename expr vars = rename_it expr [] vars
    where
        rename_it :: Eq b => Expr b -> [(b,b)] -> [b] -> (Expr b, [b])
        rename_it (Var v) renamings vars                =
            case lookup v renamings of
                Nothing -> (Var v, vars)
                Just v' -> (Var v', vars)
        rename_it (Application e1 e2) renamings vars    =
            let (e1', vars') = rename_it e1 renamings vars
                (e2', vars'') = rename_it e2 renamings vars'
            in (Application e1' e2', vars'')
        rename_it (Lambda v e) renamings (f:vars)       =
            let (e', vars') = rename_it e ((v,f):renamings) vars
            in (Lambda f e', vars')
        rename_it (Letrec env e) renamings vars =
            let (vars', renamings') = find_renamings_env env renamings vars
                (env', vars'') = rename_env env renamings' vars'
                (e', vars''') = rename_it e renamings' vars''
            in (Letrec env' e', vars'')
                where
                    find_renamings_env :: Eq b => [Assignment b] -> [(b,b)] -> [b] -> ([b], [(b,b)])
                    find_renamings_env ((Assig v e):xs) renamings (f:vars) =
                        find_renamings_env xs ((v,f):renamings) vars
                    find_renamings_env _ _ [] = error "Not enough variable names"
                    find_renamings_env [] renamings vars = (vars, renamings)
                    rename_env :: Eq b => [Assignment b] -> [(b,b)] -> [b] -> ([Assignment b], [b])
                    rename_env ((Assig v e):xs) renamings vars =
                        let (e', vars') = rename_it e renamings vars
                        in case lookup v renamings of
                            Nothing ->
                                first (Assig v e' :) (rename_env xs renamings vars')
                            Just v' ->
                                first (Assig v' e' :) (rename_env xs renamings vars')
                    rename_env _ _ [] = error "Not enough variable names"
                    rename_env [] renamings vars = ([], vars)
        rename_it (Prob e1 e2 p) renamings vars =
            let (e1', vars') = rename_it e1 renamings vars
                (e2', vars'') = rename_it e2 renamings vars'
            in (Prob e1' e2' p, vars'')
        rename_it _ _ []             =
            error "Not enough variable names"


-- |Tries to apply one reduction step, returns Nothing if no reduction is possible. Reduction Rules after `Automating the DiagramMethodto Prove Correctness of Program Transformations`, D. Sabel, Figure 5.
--The convoluted output is only neccessary for expressions containing "Prob". In that case p is the probability on the "Prob", the first expression is the left, and the second expression the right side.
--If the expression does not contain "Prob" then the Rational will always be -1.0 and both expressions will be the same.
tryNeedStd :: Eq a => Expr a -> Strategy -> Bool -> Maybe (Result (Expr a))
tryNeedStd (Application (Lambda v e1) e2) _ _ =
    Just $ Next $ Letrec [Assig v e2] e1 --lbeta
tryNeedStd (Application (Letrec env e1) e2) _ _ =
    Just $ Next $ Letrec env (Application e1 e2) --lapp
--In case of non-Letrec Application, reduce left side.
tryNeedStd (Application e1 e2) s ch =
    case tryNeedStd e1 s ch of
        Nothing
            | not ch -> Nothing
            | otherwise ->
                case tryNeedStd e2 s ch of
                    Nothing -> Nothing
                    Just (Next e2') -> Just $ Next $ Application e1 e2'
                    Just (Branch p el er) -> Just $ Branch p (Application e1 el) (Application e1 er)
        Just (Next e1') -> Just $ Next $ Application e1' e2
        Just (Branch p el er) -> Just $ Branch p (Application el e2) (Application er e2)
--If there is a sequence of variable assignments that ends in an abstraction, copy that abstraction to the first variable in the variable chain.
tryNeedStd (Letrec env e) s ch
    | hasVarChain env e s ch ch =
        case tryNeedStd e s ch of
            Nothing -> Just $ cpin env e e --cp-in
                where
                    cpin :: Eq a => [Assignment a] -> Expr a -> Expr a -> Result (Expr a)
                    cpin env e eRM
                        | not ch && hasAnyVar e =
                            case getVarChain (nextVar e) env env s False of
                                Nothing -> error "There is no VarChain"
                                Just e' ->
                                    let cp = copy e (nextVar e) e' s
                                    in Next $ Letrec env cp
                        | ch && hasAnyVarChurch eRM =
                            case getVarChain (nextVarChurch eRM) env env s True of
                                Nothing -> cpin env e (removeVar eRM)
                                Just e' ->
                                    let cp = copyChurch e (nextVarChurch eRM) e'
                                    in Next $ Letrec env cp
                        | otherwise = error "There is no VarChain"
            Just (Next e') -> Just $ Next $ Letrec env e'
            Just (Branch p el er) -> Just $ Branch p (Letrec env el) (Letrec env er)
--If there is a sequence of assignments as detailed in (sr,cp-e) of Figure 5 mentioned above, copy the abstraction to the last context in the context chain. 
tryNeedStd (Letrec env e) s ch
    | hasEnvChain env e s ch =
        case tryNeedStd e s ch of
            Nothing -> Just $ cpe env e e --cp-e
                where
                    cpe :: Eq a => [Assignment a] -> Expr a -> Expr a -> Result (Expr a)
                    cpe env e eRM
                        | not ch && hasAnyVar e =
                            case getContextChain (nextVar e) env env s of
                                Nothing -> error "There is no ContextChain"
                                Just e' ->
                                    case getContextChainNoVarChain (nextVar e) env env s of
                                        Nothing -> error "There is no VarChain"
                                        Just v ->
                                            let cpEnv = copyEnv env v e' s
                                            in Next $ Letrec cpEnv e
                        | ch && hasAnyVarChurch eRM =
                            case getContextChain (nextVarChurch eRM) env env s of
                                Nothing -> cpe env e (removeVar eRM)
                                Just e' ->
                                    case getContextChainNoVarChain (nextVarChurch eRM) env env s of
                                        Nothing -> error "There is no VarChain"
                                        Just v ->
                                            let cpEnv = copyEnvChurch env v e'
                                            in Next $ Letrec cpEnv e
                        | otherwise = error "There are no Variables in expression"
            Just (Next e') -> Just $ Next $ Letrec env e'
            Just (Branch p el er) -> Just $ Branch p (Letrec env el) (Letrec env er)
tryNeedStd (Letrec env (Letrec env2 e)) _ _ = --llet-in
    Just $ Next $ Letrec (env++env2) e
--If there is a sequence of Assignments ending in a Letrec expression being assigned, add that environment to the outer environment and only assign the expression instead of the Letrec.
tryNeedStd (Letrec env2 e) s ch
    | hasEnvChainLetrec env2 e ch =
        case tryNeedStd e s ch of
            Nothing -> Just $ llete env2 e e --llet-e
                where
                    llete :: Eq a => [Assignment a] -> Expr a -> Expr a -> Result (Expr a)
                    llete env2 e eRM
                        | not ch && hasAnyVar e =
                            case getContextChainLetrec (nextVar e) env2 env2 of
                                Nothing -> error "There is no ContextChain"
                                Just a@(Assig v (Letrec env1 e')) ->
                                    let replAssig = replaceAssig env2 v e' ++ env1
                                    in Next $ Letrec replAssig e
                                Just a -> error "Should be Letrec Assignment, debug getContextChainLetrec"
                        | ch && hasAnyVarChurch eRM =
                            case getContextChainLetrec (nextVarChurch eRM) env2 env2 of
                                Nothing -> llete env2 e (removeVar eRM)
                                Just a@(Assig v (Letrec env1 e')) ->
                                    let replAssig = replaceAssig env2 v e' ++ env1
                                    in Next $ Letrec replAssig e
                                Just a -> error "Should be Letrec Assignment, debug getContextChainLetrec"
                        | otherwise = error "There are no Variables in expression"
            Just (Next e') -> Just $ Next $ Letrec env2 e'
            Just (Branch p el er) -> Just $ Branch p (Letrec env2 el) (Letrec env2 er)
--Try reduction in the expression, if that doesn't work try the environment.
tryNeedStd (Letrec env e) s ch =
    case tryNeedStd e s ch of
        Nothing ->
            case tryNeedStdEnv env env s ch of
                Nothing -> Nothing
                Just (Next env') -> Just $ Next $ Letrec env' e
                Just (Branch p envl envr) -> Just $ Branch p (Letrec envl e) (Letrec envr e)
        Just (Next e') -> Just $ Next $ Letrec env e'
        Just (Branch p el er) -> Just $ Branch p (Letrec env el) (Letrec env er)
--Try to reduce the branches of the probabilities at first in CbV
tryNeedStd (Prob a b p) Value ch =
    case tryNeedStd a Value ch of --prob-CbV
        Nothing ->
            case tryNeedStd b Value ch of
                Nothing -> Just $ Branch p a b
                Just (Next b') -> Just $ Next $ Prob a b' p
                Just (Branch pb bl br) -> Just $ Branch pb (Prob a bl p) (Prob a br p)
        Just (Next a') -> Just $ Next $ Prob a' b p
        Just (Branch pa al ar) -> Just $ Branch pa (Prob al b p) (Prob ar b p)
--Reduce the Prob structure first in CbName and CbNeed
tryNeedStd (Prob a b p) _ _ = Just $ Branch p a b --prob-CbN
tryNeedStd (Lambda v e) s True =
    case tryNeedStd e s True of
        Nothing -> Nothing
        Just (Next e') -> Just $ Next $ Lambda v e'
        Just (Branch p el er) -> Just $ Branch p (Lambda v el) (Lambda v er)
tryNeedStd _ _ _ = Nothing

-- |Try to do a reduction step in an environment of a Letrec expression. Used in `tryNeedStd` in case no reduction rules can be applied previously.
tryNeedStdEnv :: Eq a => [Assignment a] -> [Assignment a] -> Strategy -> Bool -> Maybe (Result [Assignment a])
tryNeedStdEnv ((Assig v e):xs) old s ch =
    case tryNeedStd e s ch of
        Nothing -> tryNeedStdEnv xs old s ch
        Just (Next e') -> Just $ Next $ replaceAssig old v e'
        Just (Branch p el er) -> Just $ Branch p (replaceAssig old v el) (replaceAssig old v er)
tryNeedStdEnv [] _ _ _ = Nothing

-- |Try to completely reduce a given expression with alpha-converstion before every step.
--If the expression does not contain "Prob" then this will return a "PLeaf" containing the reduced expression.
--Otherwise the ProbTree will branch accordingly with p for the left and (1-p) for the right side, storing the split expression on the Node.
--The process will terminate after 1000 steps to prevent infinite branchings.
tryNeed :: Eq a => Expr a -> [a] -> Bool -> Bool -> Strategy -> Rational -> Integer -> ProbTree a
tryNeed e _ _ _ _ _ ctr
    | ctr > 100 = PNode e  (Left "Process terminated automatically after 100 steps")
                            (Left "Process terminated automatically after 100 steps")
tryNeed e vars True ch s pOld ctr =
    let (e', vars') = rename e vars
    in case tryNeedStd e' s ch of
        Nothing -> PLeaf (garbageCollection e') ctr
        Just (Next e'') -> tryNeed (garbageCollection e'') vars' True ch s pOld (ctr + 1)
        Just (Branch p el er)
            | p > 0 && p < 1 ->
                let pL = p*pOld
                    pR = (1-p)*pOld
                in PNode e' (Right (tryNeed (garbageCollection el) vars' True ch s pL (ctr + 1), pL))
                            (Right (tryNeed (garbageCollection er) vars' True ch s pR (ctr + 1), pR))
            | p == 0 ->
                tryNeed (garbageCollection er) vars' True ch s pOld (ctr + 1)
            | p == 1 ->
                tryNeed (garbageCollection el) vars' True ch s pOld (ctr + 1)
            | otherwise -> error "probabilities must be between 0 and 1"
tryNeed e vars False ch s pOld ctr =
    let (e', vars') = rename e vars
    in case tryNeedStd e' s ch of
        Nothing -> PLeaf e' ctr
        Just (Next e'') -> tryNeed e'' vars' False ch s pOld (ctr + 1)
        Just (Branch p el er)
            | p > 0 && p < 1 ->
                let pL = p*pOld
                    pR = (1-p)*pOld
                in PNode e' (Right (tryNeed el vars' False ch s pL (ctr + 1), pL))
                            (Right (tryNeed er vars' False ch s pR (ctr + 1), pR))
            | p == 0 ->
                tryNeed er vars' False ch s pOld (ctr + 1)
            | p == 1 ->
                tryNeed el vars' False ch s pOld (ctr + 1)
            | otherwise -> error "probabilities must be between 0 and 1"

tryNeedMCSim :: Eq a => Expr a -> [a] -> Bool -> Strategy -> Integer -> IO (EExpr a, Integer)
--tryNeedMCSim e _ _ _ ctr
--    | ctr > 1000 = return $ Left "Process terminated automatically after 1000 steps"
tryNeedMCSim e vars True s ctr =
    let (e', vars') = rename e vars
    in case tryNeedStd e' s False of
        Nothing -> return (Right $ garbageCollection e',ctr)
        Just (Next e'') -> tryNeedMCSim (garbageCollection e'') vars' True s (ctr + 1)
        Just (Branch p el er)
            | p > 0 && p < 1 ->
                do
                    r <- R.randomRIO (0.0,1.0) :: IO Double
                    if toRational r <= p
                        then tryNeedMCSim (garbageCollection el) vars' True s (ctr + 1)
                        else tryNeedMCSim (garbageCollection er) vars' True s (ctr + 1)
            | p == 0 ->
                tryNeedMCSim (garbageCollection er) vars' True s (ctr + 1)
            | p == 1 ->
                tryNeedMCSim (garbageCollection el) vars' True s (ctr + 1)
            | otherwise -> error "probabilities must be between 0 and 1"
tryNeedMCSim e vars False s ctr =
    let (e', vars') = rename e vars
    in case tryNeedStd e' s False of
        Nothing -> return (Right e', ctr)
        Just (Next e'') -> tryNeedMCSim e'' vars' False s (ctr + 1)
        Just (Branch p el er)
            | p > 0 && p < 1 ->
                do
                    r <- R.randomRIO (0.0,1.0) :: IO Double
                    if toRational r <= p
                        then tryNeedMCSim el vars' False s (ctr + 1)
                        else tryNeedMCSim er vars' False s (ctr + 1)
            | p == 0 ->
                tryNeedMCSim er vars' False s (ctr + 1)
            | p == 1 ->
                tryNeedMCSim el vars' False s (ctr + 1)
            | otherwise -> error "probabilities must be between 0 and 1"

tryNeedX :: Eq a => Expr a -> [a] -> Bool -> Strategy -> Rational -> Integer -> Integer -> ProbTree a
tryNeedX e _ _ _ _ ctr _
    | ctr > 1000 = PNode e (Left "Process terminated automatically after 1000 steps") (Left "Process terminated automatically after 1000 steps")
tryNeedX e vars True s pOld ctr thresh
    | ctr == thresh = PLeaf e ctr
    | otherwise =
        let (e', vars') = rename e vars
        in case tryNeedStd e' s False of
            Nothing -> PLeaf (garbageCollection e') ctr
            Just (Next e'') -> tryNeedX (garbageCollection e'') vars' True s pOld (ctr + 1) thresh
            Just (Branch p el er)
                | p > 0 && p < 1 ->
                    let pL = p*pOld
                        pR = (1-p)*pOld
                    in PNode e' (Right (tryNeedX (garbageCollection el) vars' True s pL (ctr + 1) thresh, pL))
                                (Right (tryNeedX (garbageCollection er) vars' True s pR (ctr + 1) thresh, pR))

                | p == 0 ->
                    tryNeedX (garbageCollection er) vars' True s pOld (ctr + 1) thresh
                | p == 1 ->
                    tryNeedX (garbageCollection el) vars' True s pOld (ctr + 1) thresh
                | otherwise -> error "probabilities must be between 0 and 1"
tryNeedX e vars False s pOld ctr thresh
    | ctr == thresh = PLeaf e ctr
    | otherwise =
        let (e', vars') = rename e vars
        in case tryNeedStd e' s False of
            Nothing -> PLeaf e' ctr
            Just (Next e'') -> tryNeedX e'' vars' False s pOld (ctr + 1) thresh
            Just (Branch p el er)
                | p > 0 && p < 1 ->
                    let pL = p*pOld
                        pR = (1-p)*pOld
                    in PNode e' (Right (tryNeedX el vars' False s pL (ctr + 1) thresh, pL))
                                (Right (tryNeedX er vars' False s pR (ctr + 1) thresh, pR))
                | p == 0 ->
                    tryNeedX er vars' False s pOld (ctr + 1) thresh
                | p == 1 ->
                    tryNeedX el vars' False s pOld (ctr + 1) thresh
                | otherwise -> error "probabilities must be between 0 and 1"

reduceNeedMCSim :: Expr StringExpr -> Bool -> Strategy -> IO (EExpr StringExpr, Integer)
reduceNeedMCSim e gc s = tryNeedMCSim e fresh gc s 0

mCSim :: Expr StringExpr -> Bool -> Strategy -> Integer -> IO ([EExpr StringExpr], Integer)
mCSim _ _ _ 0 = return ([], 0)
mCSim e gc s numRep =
    do
        (e', ctr) <- tryNeedMCSim e fresh gc s 0
        (xs', ctr') <- mCSim e gc s (numRep - 1)
        case e' of
            Left s ->
                return (Left s : xs', ctr + ctr')
            Right e'' ->
                return (Right e'' : xs', ctr + ctr')

countMC :: Eq a => [EExpr a] -> ProbDistribution a
countMC xs = traverseMC xs xs (toInteger $ length xs) []
    where
        traverseMC :: Eq a => [EExpr a] -> [EExpr a] -> Integer -> ProbDistribution a -> ProbDistribution a
        traverseMC [] _ _ res = res
        traverseMC (x:xs) old l res =
            case lookup x res of
                Nothing ->
                    traverseMC xs old l $ (x, countInList x old % l):res
                Just _ ->
                    traverseMC xs old l res

countInList :: Eq a => a -> [a] -> Integer
countInList x [] = 0
countInList x (y:ys) =
    if x == y
        then 1 + countInList x ys
        else countInList x ys

monteCarlo :: Expr StringExpr -> Bool -> Strategy -> Integer -> IO (ProbDistribution StringExpr)
monteCarlo e gc s numRep = do 
                            (xs, ctr) <- mCSim e gc s numRep
                            print $ "Average number of reduction steps: " ++ show (div ctr numRep)
                            return $ countMC xs

-- |Call `tryNeed` with `fresh` without garbage collection.
--Toggle garbage collection with `Bool` argument. Choose Evaluation Strategy for Prob with `Strategy` argument
reduceNeed :: Expr StringExpr -> Bool -> Strategy -> ProbTree StringExpr
reduceNeed e gc s = tryNeed e fresh gc False s 1 0

-- |Call `reduceNeed` and transform the result into a probability distribution with `treeToDist`
reduceNeedDist :: Expr StringExpr -> Bool -> Strategy -> ProbDistribution StringExpr
reduceNeedDist e gc s = groupDistExpr $ treeToDist $ reduceNeed e gc s

reduceNeedX :: Expr StringExpr -> Bool -> Strategy -> Integer -> ProbTree StringExpr
reduceNeedX e gc s = tryNeedX e fresh gc s 1 0

reduceNeedXDist :: Expr StringExpr -> Bool -> Strategy -> Integer -> ProbDistribution StringExpr
reduceNeedXDist e gc s thresh = treeToDist $ reduceNeedX e gc s thresh

groupDistExpr :: ProbDistribution StringExpr -> ProbDistribution StringExpr
groupDistExpr dist =    let dist' = mapRename dist
                    in groupDist' dist' dist' []
    where   groupDist' [] _ res = res
            groupDist' (ex@(e, pr):xs) old res =
                case lookup e res of
                    Nothing -> groupDist' xs old ((e, getProbSum e old):res)
                    Just p -> groupDist' xs old res

groupDistChurch :: [(Either String Integer, Rational)] -> [(Either String Integer, Rational)]
groupDistChurch dist = groupDistChurch' dist dist []
    where   groupDistChurch' [] _ res = res
            groupDistChurch' (ex@(e, pr):xs) old res =
                case lookup e res of
                    Nothing -> groupDistChurch' xs old ((e, getProbSumChurch e old):res)
                    Just p -> groupDistChurch' xs old res

getProbSumChurch :: Either String Integer -> [(Either String Integer, Rational)] -> Rational
getProbSumChurch _ [] = 0
getProbSumChurch e ((e', p):xs)
    | e == e' = p + getProbSumChurch e xs
    | otherwise = getProbSumChurch e xs

getProbSum :: EExpr StringExpr -> ProbDistribution StringExpr -> Rational
getProbSum _ [] = 0
getProbSum ee@(Right e) ((Right ex, p):xs)
    | fst (rename e fresh3) == fst (rename ex fresh3) = p + getProbSum ee xs
    | otherwise = getProbSum ee xs
getProbSum ee@(Left s) ((Left s', p):xs)
    | s == s' = p + getProbSum ee xs
    | otherwise = getProbSum ee xs
getProbSum ee (x:xs) = getProbSum ee xs

reduceChurch :: ProbDistribution StringExpr -> ProbDistribution StringExpr
reduceChurch [] = []
reduceChurch ((Right e, p):xs) =
    case tryNeed e fresh True True Need 1 0 of
        PLeaf e' _ -> (Right e', p) : reduceChurch xs
        PNode _ (Left s) (Left s') -> (Left s, p) : reduceChurch xs
        PNode {} -> error "This is not a church numeral"
reduceChurch (x:xs) = x : reduceChurch xs

churchToInt :: Expr a -> Integer
churchToInt (Lambda v e) = churchToInt e
churchToInt (Application _ (Application e1 e2)) = 1 + churchToInt (Application e1 e2)
churchToInt (Application _ _) = 1
churchToInt (Var _) = 0
churchToInt _ = error "This is not a church numeral"

churchToIntNeed :: Eq a => Expr a -> [Assignment a] -> Integer
churchToIntNeed (Letrec env e) old = churchToIntNeedEnv env env + 1
churchToIntNeed (Lambda v (Lambda w (Lambda y e))) _ = 0
churchToIntNeed (Lambda v (Lambda w e)) _ = countVar v e
churchToIntNeed (Application e1 e2) env =
    checkForSucc e1 env True + checkForSucc e2 env False
churchToIntNeed _ _ = 0

checkForSucc :: Eq a => Expr a -> [Assignment a] -> Bool -> Integer
checkForSucc (Var v) env True =
    case getVarChain v env env Need False of
        Just (Lambda _ (Lambda _ (Lambda _ _))) -> 1
        _ -> 0
checkForSucc (Application e1 e2) env _ =
    checkForSucc e1 env True + checkForSucc e2 env False
checkForSucc _ _ _ = 0

countVar :: Eq a => a -> Expr a -> Integer
countVar v (Var v') | v == v' = 1
                    | otherwise = 0
countVar v (Application e1 e2) = countVar v e1 + countVar v e2
countVar _ _ = 0

churchToIntNeedEnv :: Eq a => [Assignment a] -> [Assignment a] -> Integer
churchToIntNeedEnv [] _ = 0
churchToIntNeedEnv ((Assig v e) : xs) old = churchToIntNeed e old + churchToIntNeedEnv xs old

mapChurchToInt :: Eq a => ProbDistribution a -> [(Either String Integer, Rational)]
mapChurchToInt [] = []
mapChurchToInt ((Right e, p):xs) = (Right $ churchToInt e, p) : mapChurchToInt xs
mapChurchToInt ((Left s, p):xs) = (Left s, p) : mapChurchToInt xs

mapChurchToIntNeed :: Eq a => ProbDistribution a -> [(Either String Integer, Rational)]
mapChurchToIntNeed [] = []
mapChurchToIntNeed ((Right e, p):xs) = (Right $ churchToIntNeed e [], p) : mapChurchToIntNeed xs
mapChurchToIntNeed ((Left s, p):xs) = (Left s, p) : mapChurchToIntNeed xs

mapRename :: ProbDistribution StringExpr -> ProbDistribution StringExpr
mapRename [] = []
mapRename ((Right e, p):xs) =
    let (e', v) = rename e fresh2
    in (Right e', p): mapRename xs
mapRename (x:xs) = x : mapRename xs

-- |Convert a probability tree into a probability distibution by collecting all leaves with their probability.
treeToDist :: ProbTree a -> ProbDistribution a
treeToDist (PNode _ (Right (PNode _ (Left s1)
                                    (Left s2), pll))
                    (Right (PLeaf e _, p))) =
    [(Left s1, pll), (Right e, p)]
treeToDist (PNode _ (Right (PLeaf e _, p))
                    (Right (PNode _ (Left s1)
                                    (Left s2), pll))) =
    [(Left s1, pll), (Right e, p)]
treeToDist (PNode _ (Right (PNode _ (Left s1)
                                    (Left s2), pll))
                    (Right (t@(PNode _  (Right _)
                                        (Right _)), _))) =
    (Left s1, pll) : treeToDist t
treeToDist (PNode _ (Right (t@(PNode _  (Right _)
                                        (Right _)), _))
                    (Right (PNode _ (Left s1)
                                    (Left s2), pll))) =
    (Left s1, pll) : treeToDist t
treeToDist (PNode _ (Right (PNode _ (Left s1)
                                    (Left s2), pl))
                    (Right (PNode _ (Left s3)
                                    (Left s4), pr))) =
    [(Left s1, pl), (Left s3, pr)]
treeToDist (PNode _ (Right (PLeaf el _, pl))
                    (Right (PLeaf er _, pr))) =
    [(Right el, pl), (Right er, pr)]
treeToDist (PNode _ (Right (PLeaf el _, pl))
                    (Right (tr, _))) =
    (Right el, pl) : treeToDist tr
treeToDist (PNode _ (Right (tl, _))
                    (Right (PLeaf er _, pr))) =
    (Right er, pr) : treeToDist tl
treeToDist (PNode _ (Right (tl, _))
                    (Right (tr, _))) =
    treeToDist tr ++ treeToDist tl
treeToDist (PNode _ (Left s1)
                    (Left s2)) =
    [(Left s1, 1)]
treeToDist (PLeaf e _) =
    [(Right e, 1)]
treeToDist _ = error "This Tree should not be possible"

-- |Delete unused assignments in Letrec environments
garbageCollection :: Eq a => Expr a -> Expr a
garbageCollection (Letrec env e) =
    case checkEnv env env e of
        [] -> garbageCollection e                   --gc2
        env'    | env == env'   -> Letrec (garbageCollectionEnv env') (garbageCollection e)   --gc1
                | otherwise     -> garbageCollection $ Letrec env' e
garbageCollection (Var v) = Var v
garbageCollection (Lambda v e) = Lambda v $ garbageCollection e
garbageCollection (Application e1 e2) = Application (garbageCollection e1)
                                                    (garbageCollection e2)
garbageCollection (Prob e1 e2 p) = Prob (garbageCollection e1)
                                        (garbageCollection e2)
                                        p

garbageCollectionEnv :: Eq a => [Assignment a] -> [Assignment a]
garbageCollectionEnv [] = []
garbageCollectionEnv ((Assig v e):xs) = Assig v (garbageCollection e) : garbageCollectionEnv xs

-- |Check for all assignments in the environment if the assignment is used in the rest of the environment or the expression. If the assignment is used, keep it, else discard it.
checkEnv :: Eq a => [Assignment a] -> [Assignment a] -> Expr a -> [Assignment a]
checkEnv (a@(Assig v _):xs) old e =
    if hasVarEnv v old || hasSpecVar v e
        then a : checkEnv xs old e
        else checkEnv xs old e
checkEnv [] _ _ = []

-- |Recursively check if the specific variable is used in the expression and if that is the case return True.
hasSpecVar :: Eq a => a -> Expr a -> Bool
hasSpecVar v (Var w)    | v == w = True
                        | otherwise = False
hasSpecVar v (Lambda _ e) = hasSpecVar v e
hasSpecVar v (Application e1 e2) = hasSpecVar v e1 || hasSpecVar v e2
hasSpecVar v (Letrec env e) = hasVarEnv v env || hasSpecVar v e
hasSpecVar v (Prob e1 e2 _) = hasSpecVar v e1 || hasSpecVar v e2

-- |Return True if any assigned expression uses the specific variable.
hasVarEnv :: Eq a => a -> [Assignment a] -> Bool
hasVarEnv v ((Assig _ e):xs) = hasSpecVar v e || hasVarEnv v xs
hasVarEnv _ [] = False

-- |Substitute a specific variable for a specific expression. Called in `copyEnv` and the cp-in case of `tryNeedStd`.
copy :: Eq a => Expr a -> a -> Expr a -> Strategy -> Expr a
copy ex@(Var w) v e' _
    | v == w = e'
    | otherwise = ex
copy ex@(Lambda w e) v e' _ = ex
copy (Application e1 e2) v e' s = case s of
    Name -> Application (copy e1 v e' s) (copy e2 v e' s)
    _ -> Application (copy e1 v e' s) e2
copy (Letrec env e) v e' s = Letrec env $ copy e v e' s
copy ex@(Prob e1 e2 p) v e' s = ex

copyChurch :: Eq a => Expr a -> a -> Expr a -> Expr a
copyChurch ex@(Var w) v e'
    | v == w = e'
    | otherwise = ex
copyChurch (Lambda w e) v e' = Lambda w (copyChurch e v e')
copyChurch (Application e1 e2) v e' = Application (copyChurch e1 v e') (copyChurch e2 v e')
copyChurch (Letrec env e) v e' = Letrec env $ copyChurch e v e'
copyChurch ex@(Prob e1 e2 p) v e' = ex

-- |Substitute a specific variable for a specific expression withing the environment of a Letrec expression. Called in the cp-e case of `tryNeedStd`.
copyEnv :: Eq a => [Assignment a] -> a -> Expr a -> Strategy -> [Assignment a]
copyEnv (a@(Assig y (Var w)):xs) v e' s
    | v == w = Assig y e' : copyEnv xs v e' s
    | otherwise = a : copyEnv xs v e' s
copyEnv (a@(Assig y (Lambda z e)):xs) v e' s =
    a : copyEnv xs v e' s
copyEnv ((Assig y (Application e1 e2)):xs) v e' s =
    case s of
        Name -> Assig y (Application (copy e1 v e' s) (copy e2 v e' s)) : copyEnv xs v e' s
        _ -> Assig y (Application (copy e1 v e' s) e2) : copyEnv xs v e' s
copyEnv ((Assig y (Letrec env e)):xs) v e' s =
    Assig y (Letrec env $ copy e v e' s) : copyEnv xs v e' s
copyEnv (a@(Assig y (Prob e1 e2 p)):xs) v e' s =
    a : copyEnv xs v e' s
copyEnv [] _ _ _ = []

copyEnvChurch :: Eq a => [Assignment a] -> a -> Expr a -> [Assignment a]
copyEnvChurch (a@(Assig y (Var w)):xs) v e'
    | v == w = Assig y e' : copyEnvChurch xs v e'
    | otherwise = a : copyEnvChurch xs v e'
copyEnvChurch ((Assig y (Lambda z e)):xs) v e' =
    Assig y (Lambda z $ copyChurch e v e') : copyEnvChurch xs v e'
copyEnvChurch ((Assig y (Application e1 e2)):xs) v e' =
    Assig y (Application (copyChurch e1 v e') (copyChurch e2 v e')) : copyEnvChurch xs v e'
copyEnvChurch ((Assig y (Letrec env e)):xs) v e' =
    Assig y (Letrec env $ copyChurch e v e') : copyEnvChurch xs v e'
copyEnvChurch (a@(Assig y (Prob e1 e2 p)):xs) v e' =
    a : copyEnvChurch xs v e'
copyEnvChurch [] _ _ = []


-- |Replace the expression of a specific assignment with another expression. Called in the llet-e case of `tryNeedStd`.
replaceAssig :: Eq a => [Assignment a] -> a -> Expr a -> [Assignment a]
replaceAssig (a@(Assig w e):xs) v e'
    | v == w = Assig w e' : replaceAssig xs v e'
    | otherwise = a : replaceAssig xs v e'
replaceAssig [] _ _ = []

-- |Predicate that calls `getContextChain` for all variables in a specific expression and returns True if one such chain exists. Called in the cp-e case of `tryNeedStd`.
hasEnvChain :: Eq a => [Assignment a] -> Expr a -> Strategy -> Bool -> Bool
hasEnvChain env e s ch
    | not ch && hasAnyVar e =
        case getContextChain (nextVar e) env env s of
            Nothing -> False
            Just ex -> True
    | ch && hasAnyVarChurch e =
        case getContextChain (nextVarChurch e) env env s of
            Nothing -> hasEnvChain env (removeVar e) s ch
            Just ex -> True
    | otherwise = False

-- |Traverse environment to look for a sequence of assignments as required in the (sr,cp-e) case of standard reduction in Figure 5 of the paper referenced at `tryNeedStd`.
--Once the Assignment for the starting variable is found, `getSpecContextChain` is called to look for a context chain starting from the assigned expression.
--Returns `Nothing` if no context chain is found. Called in the cp-e case of `tryNeedStd` and in `hasEnvChain`.
getContextChain :: Eq a => a -> [Assignment a] -> [Assignment a] -> Strategy -> Maybe (Expr a)
getContextChain v ((Assig w (Var z)):xs) old s
    | v == w =
        case getContextChain z old old s of
            Nothing -> getContextChain v xs old s
            Just e -> Just e
getContextChain v ((Assig w e@(Lambda _ _)):xs) old s
    | v == w = Just e
getContextChain v ((Assig w e@(Prob _ _ _)):xs) old Name
    | v == w = Just e
getContextChain v ((Assig w e):xs) old s
    | v == w =
        if hasAnyVar e
            then
                case getSpecContextChain e old old s of
                    Nothing -> getContextChain v xs old s
                    Just ex -> Just ex
            else getContextChain v xs old s
    | otherwise = getContextChain v xs old s
getContextChain _ [] _ _ = Nothing


-- |Traverse environment for all variables of a specific expression until a context chain is found, or until no variables are left. In the later case return `Nothing`.
--Called in `getContextChain`.
getSpecContextChain :: Eq a => Expr a -> [Assignment a] -> [Assignment a] -> Strategy -> Maybe (Expr a)
getSpecContextChain eRM ((Assig w (Var _)):xs) old s
    | w == nextVar eRM = getVarChain w old old s True
getSpecContextChain eRM ((Assig w e@(Lambda _ _)):xs) old s
    | w == nextVar eRM = Just e
getSpecContextChain eRM ((Assig w e@(Prob _ _ _)):xs) old Name
    | w == nextVar eRM = Just e
getSpecContextChain eRM ((Assig w e):xs) old s
    | w == nextVar eRM =
        if hasAnyVar e
            then getSpecContextChain e old old s
            else getSpecContextChain eRM xs old s
    | otherwise = getSpecContextChain eRM xs old s
getSpecContextChain _ [] _ _ = Nothing

{-
getContextChainChurch :: Eq a => Expr a -> [Assignment a] -> Strategy -> Maybe (Expr a)
getContextChainChurch eRM old s =
                                if hasAnyVarChurch eRM
                                    then 
                                        case getContextChain (nextVarChurch eRM) old old s True of
                                            Nothing -> getContextChainChurch (removeVar eRM) old s
                                            Just ex -> Just ex
                                    else Nothing
-}

-- |Traverse environment to check for the context chain found with `getContextChain`, without the possible variable chain in the end.
--Used to find the variable that needs to be substituted in the cp-e case of `tryNeedStd`.
getContextChainNoVarChain :: Eq a => a -> [Assignment a] -> [Assignment a] -> Strategy -> Maybe a
getContextChainNoVarChain v ((Assig w (Var z)):xs) old s
    | v == w =
        case getContextChainNoVarChain z old old s of
            Nothing -> getContextChainNoVarChain v xs old s
            Just y -> Just y
getContextChainNoVarChain v ((Assig w e@(Lambda _ _)):xs) old s
    | v == w = Just w
getContextChainNoVarChain v ((Assig w e@(Prob _ _ _)):xs) old Name
    | v == w = Just w
getContextChainNoVarChain v ((Assig w e):xs) old s
    | v == w =
        if hasAnyVar e
            then
                case getSpecContextChainNoVarChain e old old s of
                    Nothing -> getContextChainNoVarChain v xs old s
                    Just ex -> Just ex
            else getContextChainNoVarChain v xs old s
    | otherwise = getContextChainNoVarChain v xs old s
getContextChainNoVarChain _ [] _ _ = Nothing

{-
getContextChainNoVarChainChurch :: Eq a => Expr a -> [Assignment a] -> Strategy -> Maybe a
getContextChainNoVarChainChurch eRM old s =
                                if hasAnyVarChurch eRM
                                    then 
                                        case getContextChainNoVarChain (nextVarChurch eRM) old old s True of
                                            Nothing -> getContextChainNoVarChainChurch (removeVar eRM) old s
                                            Just ex -> Just ex
                                    else Nothing
-}

-- |Behaves like `getSpecContextChain` except that it doesn't call `getVarChain` but instead returns the variable the variable chain would start with.
--Called in `getContextChainNoVarChain`.
getSpecContextChainNoVarChain :: Eq a => Expr a -> [Assignment a] -> [Assignment a] -> Strategy -> Maybe a
getSpecContextChainNoVarChain eRM ((Assig w (Var _)):xs) old s
    | w == nextVar eRM = Just w
getSpecContextChainNoVarChain eRM ((Assig w e@(Lambda _ _)):xs) old s
    | w == nextVar eRM = Just w
getSpecContextChainNoVarChain eRM ((Assig w e@(Prob _ _ _)):xs) old Name
    | w == nextVar eRM = Just w
getSpecContextChainNoVarChain eRM ((Assig w e):xs) old s
    | w == nextVar eRM =
        if hasAnyVar e
            then getSpecContextChainNoVarChain e old old s
            else getSpecContextChainNoVarChain eRM xs old s
    | otherwise = getSpecContextChainNoVarChain eRM xs old s
getSpecContextChainNoVarChain _ [] _ _ = Nothing

-- |Predicate that calls `getContextChainLetrec` for all variables in a specific expression and returns True if one such chain exists. Called in the llet-e case of `tryNeedStd`.
hasEnvChainLetrec :: Eq a => [Assignment a] -> Expr a -> Bool -> Bool
hasEnvChainLetrec env e ch
    | not ch && hasAnyVar e =
        case getContextChainLetrec (nextVar e) env env of
            Nothing -> False
            Just a -> True
    | ch && hasAnyVarChurch e =
        case getContextChainLetrec (nextVarChurch e) env env of
            Nothing -> hasEnvChainLetrec env (removeVar e) ch
            Just a -> True
    | otherwise = False

-- |Traverse environment to look for a context chain ending in a Letrec-expression, starting from a specific variable.
--Called in the llet-e case of `tryNeedStd` and in `hasEnvChainLetrec`.
getContextChainLetrec :: Eq a => a -> [Assignment a] -> [Assignment a] -> Maybe (Assignment a)
getContextChainLetrec v ((Assig w (Var u)):xs) old
    | v == w =
        case getContextChainLetrec u old old of
            Nothing -> getContextChainLetrec v xs old
            Just a -> Just a
    | otherwise = getContextChainLetrec v xs old
getContextChainLetrec v (a@(Assig w (Letrec _ _)):xs) old
    | v == w = Just a
    | otherwise = getContextChainLetrec v xs old
getContextChainLetrec v ((Assig w e):xs) old
    | v == w =
        if hasAnyVar e
            then
                case getSpecContextChainLetrec e old old of
                    Nothing -> getContextChainLetrec v xs old
                    Just a -> Just a
            else getContextChainLetrec v xs old
    | otherwise = getContextChainLetrec v xs old
getContextChainLetrec _ [] _ = Nothing

-- |Traverse environment for all variables of a specific expression until a context chain ending in a Letrec-expression is found, or until no variables are left. Then return the assignment or `Nothing`.
--Called in `getContextChainLetrec`.
getSpecContextChainLetrec :: Eq a => Expr a -> [Assignment a] -> [Assignment a] -> Maybe (Assignment a)
getSpecContextChainLetrec eRM (a@(Assig w (Letrec _ _)):xs) old
    | w == nextVar eRM = Just a
getSpecContextChainLetrec eRM ((Assig w e):xs) old
    | w == nextVar eRM =
        if hasAnyVar e
            then getSpecContextChainLetrec e old old
            else getSpecContextChainLetrec eRM xs old
    | otherwise = getSpecContextChainLetrec eRM xs old
getSpecContextChainLetrec eRM [] old = Nothing

-- |Predicate that calls `getVarChain` for all variables in a specific expression and returns True if one such chain exists. Called in the cp-in case of `tryNeedStd`.
hasVarChain :: Eq a => [Assignment a] -> Expr a -> Strategy -> Bool -> Bool -> Bool
hasVarChain env e s cont ch
    | not ch && hasAnyVar e =
        case getVarChain (nextVar e) env env s cont of
            Nothing -> False
            Just ex -> True
    | ch && hasAnyVarChurch e =
        case getVarChain (nextVarChurch e) env env s cont of
            Nothing -> hasVarChain env (removeVar e) s cont ch
            Just ex -> True
    | otherwise = False

-- |Returns the value of the next variable of the expression, going from left to right.
nextVar :: Expr a -> a
nextVar (Var v) = v
nextVar (Lambda _ e) = undefined
nextVar (Application e1 e2) = nextVar e1
nextVar (Letrec _ e) = nextVar e
nextVar (Prob e1 e2 _) = undefined

nextVarChurch :: Expr a -> a
nextVarChurch (Var v) = v
nextVarChurch (Lambda _ e) = nextVarChurch e
nextVarChurch (Application e1 e2) =
    if hasAnyVarChurch e1
        then nextVarChurch e1
        else nextVarChurch e2
nextVarChurch (Letrec _ e) = nextVarChurch e
nextVarChurch (Prob e1 e2 _) = undefined
nextVarCHurch RM = undefined

-- |Replaces what would be the result of `nextVar` with the RM constructor in order to get a new variable from `nextVar`.
removeVar :: Expr a -> Expr a
removeVar (Var v) = RM
removeVar ex@(Lambda v e) = Lambda v (removeVar e)
removeVar e@(Application e1 e2) =
    if hasAnyVarChurch e1
        then Application (removeVar e1) e2
        else Application e1 (removeVar e2)
removeVar (Letrec env e) = Letrec env $ removeVar e
removeVar e@(Prob e1 e2 _) = e
removeVar RM = RM

-- |Predicate that returns True if there is any variable left in the expression.
hasAnyVar :: Expr a -> Bool
hasAnyVar (Var _) = True
hasAnyVar (Lambda _ _) = False
hasAnyVar (Application e1 e2) = hasAnyVar e1
hasAnyVar (Letrec _ e) = hasAnyVar e
hasAnyVar (Prob {}) = False

hasAnyVarChurch :: Expr a -> Bool
hasAnyVarChurch (Var _) = True
hasAnyVarChurch (Lambda _ e) = hasAnyVarChurch e
hasAnyVarChurch (Application e1 e2) = hasAnyVarChurch e1 || hasAnyVarChurch e2
hasAnyVarChurch (Letrec _ e) = hasAnyVarChurch e
hasAnyVarChurch (Prob {}) = False
hasAnyVarChurch RM = False

-- |Traverse environment to find the first assignment of the given variable and call checkChain if it's assigned to another variable.
--old is a copy of the original list so it can be traversed to find a chain that continues in a part of the list ahead of the assignment of a.
getVarChain :: Eq a => a
                        -> [Assignment a]
                            -> [Assignment a]
                                -> Strategy
                                    -> Bool
                                        -> Maybe (Expr a)
getVarChain _ [] _ _ _ = Nothing
getVarChain v ((Assig v' (Var w)):xs) old s cont
    | v == v' =
        case getVarChain w old old s cont of
            Nothing
                | cont -> Just (Var w)
                | otherwise -> Nothing
            Just ex -> Just ex
    | otherwise = getVarChain v xs old s cont
getVarChain v ((Assig v' e@(Lambda _ _)):xs) old _ _
    | v == v' = Just e
getVarChain v ((Assig v' e@(Prob {})):xs) old Name _
    | v == v' = Just e
getVarChain v (x:xs) old s cont = getVarChain v xs old s cont
